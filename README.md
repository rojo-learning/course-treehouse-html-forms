# Treehouse - HTML Forms #

This repository contains notes and practice examples from **HTML Forms**, imparted by Nick Pettit at [Threehouse][JF].

> The web is a two-way communication medium. There’s lots of HTML elements for displaying data and producing output, and conversely, there’s also lots of HTML elements for accepting input. Accepting input from the user means creating web forms. In this course, we’ll learn about all the most important form elements that web professionals use on a daily basis.

## Contents ##

- **Form Basics**: To learn about forms, we’re going to create a simple sign up form for an imaginary web app. Our form won’t actually submit anywhere, since that requires additional server-side code. However, we will learn about all the most important HTML form elements.
- **Organizing Forms**: A form can be created with just a form element and some controls, but it's helpful to the user if the form is organized with labels and fieldset elements.
- **Choosing Options**: Sometimes when creating a form, it's better for the user to choose from predefined options rather than typing in text. This can be accomplished with select menus, radio buttons, and checkboxes.

---
This repository contains code examples from Treehouse. These are included under fair use for showcasing purposes only. Those examples may have been modified to fit my particular coding style.

[JF]: http://teamtreehouse.com