
# Organizing Forms

## The Label Element

`<label>` - The label element helps to organize forms by assigning some text to a form control. This text helps the user to understand what kind of data they should add to each form field.

```html
<form>
    <label for="text">Text:</label>   
    <input type="text" id="text" name="user_text">
</form>
```

The `for` attribute references the _id_ of the input that the label is matching.

## Field Sets and Legends

`<fieldset>` - The fieldset element wraps multiple elements into common groups. This can help organize a form and make it easier to understand for users.

`<legend>` - The legend element is similar to the label element, but instead of labeling a form control, it labels a fieldset. Adding a legend to a fieldset can provide some helpful context for users that are filling out a form.

```html
<form>
    <fieldset>
        <legend>Group 1</legend>
        
        <label for="text">Text:</label>   
        <input type="text" id="text" name="user_text">
    </fieldset>
    
    <fieldset>
        <legend>Group 2</legend>
        
        <label for="text2">Text:</label>   
        <input type="text" id="text2" name="user_text2">
    </fieldset>
</form>
```
