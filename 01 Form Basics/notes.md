# Form Basics

## The Form Element

`<form>` - The form element wraps a section of the document that contains form controls.

```html
<form action="http://url/" method="post">
  <!-- form controls here -->
</form>
```

The `action` attribute specifies the web addresses of a program that processes the information cubmited via the form.

The `method` attribute specifies the HTTP method that the browswer should use to submit the form, such as POST or GET.

Forms can not be nested inside one other.

## The Input Element

`<input>` - The input element is used to create many different types of form controls.

```html
<form>
    <input type="text" name="user_text">
</form>
```

The `type` attribute specifies what kind of form control should be rendered, such as text, email, passwords, and more.

The `name` attribute is submitted with form data so that server side can parse the information.

Most used types are...

- **checkbox**: A markable checkbox with a predefined value on its attributes.
- **date**: A control for entering a date.
- **email**: A fiel for edition email addresses.
- **file**: A control that lets the user to select a file.
- **hidden**: A control not displayed but with a value that will be submited.
- **number**: A control to enter a floating point number.
- **password**: A single line of text whose value is obscured while typing.
- **radio**: A radio button that is used in groups. Has a predefined value and shares the same name attribute on the group.
- **tel**: A control to enter a telehone number.
- **text**: A single line of text.

## The Textarea Element

`<textarea>` - The textarea element accepts multiple lines of text from the user. Mos browsers allow the resizing of this element by the user.

```html
<form>
    <textarea name="user_longtext" id="longtext" cols="30" rows="10"></textarea>
</form>
```

## The Button Element

`<button>` - This element will render a clickable button. The type attribute specifies whether the button should _submit_ the form data, _reset_ the form, or have no default behaviour for use with JavaScript.

```html
<form>
    <button type="submit"></button>
</form>
```
