
# Choosing Options

## Select Menus

`<select>` - The select element renders a drop-down menu that contains selectable options. This type of form control is ideal for scenarios where the user must choose one option from a preset list of 5 or more choices.

`<option>` - The option element represents one of the choices that a user can choose in a select menu. It should always be nested inside _select_ element.

`<optgroup>` - Wraps one or more options elements and help to create logical groups. The label attribute specifies the text that the optgroup should display above the nested options.

```html
<form action="">
    <select name="" id="">
        <optgroup label="">
            <option value=""></option>
            <option value=""></option>
        </optgroup>

        <optgroup label="">
            <option value=""></option>
        </optgroup>
    </select>
</form>
```

## Radio Buttons

Radio buttons provide the same functionality that `select` menues, but are a better option for 5 or les options.

They are defined as inputs of the `radio` type, that share the same name attribute. Only one radio button can be selected for each group.

```html
<form action="">
    <input type="radio" value="value1" id='option1' name="group1"><label for="option1">Value 1</label>
    <input type="radio" value="value2" id='option2' name="group1"><label for="option2">Value 2</label>
    <input type="radio" value="value3" id='option3' name="group1"><label for="option3">Value 3</label>
</form>
```

## Checkboxes

This element allows to select a variable number of options from multiple items.

```html
<form action="">
    <input type="checkbox" value="value1" id='option1' name="group1"><label for="option1">Value 1</label>
    <input type="checkbox" value="value2" id='option2' name="group1"><label for="option2">Value 2</label>
    <input type="checkbox" value="value3" id='option3' name="group1"><label for="option3">Value 3</label>
</form>
```
